<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('questions', 'QuestionsController')->except('show'); //because we dont want slug in show method instead of question-id


/**
 * All routes of questions takes question-id in their routes if needed  and then they provide it to QuestionsController an object of Question,
 * But when we want to use slug, routes have slug but we want object of Question in QuestionsController so we first process this slug and get corresponding Question Object and then return it to the QuestionsController.
 * For that we have to first register it in the routeprovider.
 */

Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show'); //for route binding of this we have to add code in provider:
Route::resource('questions.answers', 'AnswersController')->except(['index', 'show', 'create']);
Route::put('answers/{answer}/best-answer', 'AnswersController@bestAnswer')->name('answers.bestAnswer');


Route::post('questions/{question}/favorite', 'FavoritesController@store')->name('questions.favorite');
Route::delete('questions/{question}/unfavorite', 'FavoritesController@destroy')->name('questions.unfavorite');


//routes for votes-questions:
Route::post('questions/{question}/vote/{vote}', 'VotesController@voteQuestion')->name('questions.vote');
Route::post('answers/{answer}/vote/{vote}', 'VotesController@voteAnswer')->name('answers.vote');


Route::get('users/notifications', 'UsersController@notifications')->name('users.notifications');
