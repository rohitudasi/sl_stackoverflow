<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); //yeh hi shared hai yeh fixed hai hoge he.
            $table->unsignedBigInteger('vote_id'); //ismai yah toh question_id hoge yah answer_id.
            $table->string('vote_type'); //ismai path hoga modal class ka jiska vote_id hai.
            $table->tinyInteger('vote'); // ismai yah toh +1 hoga yah -1.
            $table->timestamps();

            //constrants:

            // [user_id, question_id/answer_id] can be duplicate. user 1 can comment on question id 1 as well as user with id can comment answer od id 1.
            // so to avoid above problem we have to keep 3rd id type to distunguish records. ['user_id','question_id/answer_id','type(model_name)'].
            $table->unique(['user_id', 'vote_id', 'vote_type']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
