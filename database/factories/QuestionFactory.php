<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title' => rtrim($faker->sentence(rand(5, 10)), '.'),
        'body' =>  $faker->paragraphs(rand(3, 7), true),
        'views_count' => rand(0, 10),
        // 'answers_count' => rand(0, 10), // we can't enter value of answers count here as we use rand() function to generate answer of paticluar question
        //so we first add event listener on Answer class (event on answer will invoke when answer instance is created then we wil incremenet the answer count of that question).
        'votes_count' => rand(-10, 10),
        // we inject user id in seeder
    ];
});
