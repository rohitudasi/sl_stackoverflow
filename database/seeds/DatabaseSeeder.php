<?php

use Illuminate\Database\Seeder;
use App\Question;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        /* factory(App\User::class, 5)->create()->each(function ($user) {

            for ($i = 1; $i <= rand(5, 10); $i++) {
                $user->questions()->create(factory(App\Question::class)->make()->toArray()); //we have used make() function here instead of craete() because create() function will diractly added created user in database but here we dont want to directly add question to db instead we want object of question here and later question will be added  using $user->questions().
            }
        }); //end of factory function

        */

        factory(\App\User::class, 5)->create()
            ->each(function ($user) {
                // single user is created.
                //we have to create questions using user,
                //and we have to create answers using question


                $user->questions()
                    ->saveMany(factory(App\Question::class, rand(2, 5))->make())
                    ->each(function ($question) {
                        $question->answers()->saveMany(factory(App\Answer::class, rand(2, 7))->make());
                    });
            });
    }
}
