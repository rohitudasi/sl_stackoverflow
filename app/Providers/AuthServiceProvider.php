<?php

namespace App\Providers;

use App\Policies\AnswerPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',

        'App\Question' => 'App\Policies\QuestionsPolicy',
        // Question::class  =>  QuestionsPolicy::class, //TODO to check why this way is not working!
        // another way to register
        Answer::class => AnswerPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        // GATE::define('update-question', function ($user, $question) {
        //     return $user->id === $question->user_id;
        // });
        // GATE::define('delete-question', function ($user, $question) {
        //     return $user->id === $question->user_id && $question->answers_count === 0;
        // });
    }
}
