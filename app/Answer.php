<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $fillable = [
        'body',
        'user_id'
    ];


    public function  getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }
    public function getBestAnswerStatusAttribute()
    {
        if ($this->id === $this->question->best_answer_id) {
            return "text-success";
        }
        return "text-dark";
    }
    public function getIsBestAttribute()
    {
        return $this->id === $this->question->best_answer_id;
    }




    /********************************************************************REGISTERING THE EVENTS**************************************************************/

    //WE DONOT HAVE TO DO ANYTHING TO REGISTER EVENTS OF THE MODEL, ONLY WE HAVE TO REGISTER HERE:
    public static function boot()
    {
        parent::boot();

        /**
         * This function wil invoke callback function when instance of Answer class is created so we can incremenet the answers_count of that question in questions table.
         */
        static::created(function ($answer) {
            $answer->question->increment('answers_count');
        });

        /**
         * This function will be called when instance of Answer class is deleted so that we can decremenet the answers count.
         */
        static::deleted(function ($answer) {
            $answer->question->decrement('answers_count');
        });
    }

    /*******************************************************************END REGISTERING THE EVENTS**************************************************************/




    /********************************************************************RELATIONSHIP FUNCTIONS**************************************************************/

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id'); //Second argument is 'foreign-key' we have to specify here because by default it will take functionname_id but foreign key is not author_id , forign key is 'user_id' we can make the function name as user then we dont need to map the forign key it will then take automatically.
    }
    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    /********************************************************************RELATIONSHIP FUNCTIONS**************************************************************/

    public function vote(int $vote)
    {
        //insert the record in the bridging table:
        $this->votes()->attach(auth()->id(), ['vote' => $vote]);

        //add or subtract the count into the votes_count of question table:
        if ($vote < 0) {
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
        }
    }


    /**
     * This function is used to update the user vote (for example: votes_count = 10 of questions table  and user already up-voted and now down voting so question is down voted by 2  and vice versa for already down-voted and now up-voting so question is upvoted by 2 (1 for user undo his action and 1 for he is upvoting ))
     *
     * @param integer $vote
     * @return void
     */
    public function updateVote(int $vote)
    {
        //update the record:
        $this->votes()->updateExistingPivot(auth()->id(), ['vote' => $vote]); //updateExistingPivot() is used when we want to update the column of bridging table except from user_id and vote_id.
        if ($vote < 0) {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }
}
