<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    //
    protected $guarded = [];


    /*****************************************************ACCESSORS**************************************************************/

    public function getUrlAttribute() //this function in invoked whenever we hit $question->url. (currently we are using thsi function to give a single-post's link)
    {
        return "questions/{$this->slug}";
    }


    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * This function is used to check the conditions of the answer (if the answer is answered,unanswered or is_best_answer) and return the style according to it.
     *
     * @return void
     */
    public function getAnswersStyleAttribute()
    {
        if ($this->answers_count > 0) {
            if ($this->best_answer_id) {
                return "has-best-answer";
            }
            return "answered";
        }
        return "unanswered";
    }


    /**
     * This function retuns the favorites count of this question.
     *
     * @return int
     */
    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }


    /**
     * This function returns true if this question is favorite for loggedin user
     *
     * @return boolean
     */
    public function getIsFavoriteAttribute()
    {
        return $this->favorites()->where(['user_id' => auth()->id()])->count() > 0;
    }
    /******************************************************END OF ACCESSORS**************************************************************/






    /*************************************************************MUTATORS**************************************************************/


    /**
     * MUTATORS: These are special functions which have set***Attribute($value)
     * It is a magic fuction which invoked whenever we set attribute associated with this function:
     * for:Eg if we set setNameAttribute($value) then this function will be called whenever we set Name.
     */

    /**
     * This function is invoked whenever we set the value of title, at that time we will also make the slug from title and store both of them into $this=>attributes array.
     *
     * @param [type] $title
     * @return void
     */
    public function setTitleAttribute($title)
    {

        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }


    /******************************************************END OF MUTATORS**************************************************************/





    /******************************************************RELATIONSHIP FUNCTIONS DEFINATION**************************************************************/


    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id'); //here we have to specify the foreign key otherwise it will take function name as foreign key: for example it consider forign key as (owner) but our foreign key name is 'user_id' so we have mentioned

    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }


    //polymorphic many to many relationship

    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }
    /******************************************************END OF RELATIONSHIP FUNCTIONS DEFINATION**************************************************************/




    /********************************************************************HELPER FUNCTIONS**************************************************************/

    public function undoBestAnswer()
    {
        $this->best_answer_id = null;
        $this->save();
    }

    public function markBestAnswer(Answer $answer)
    {
        $this->best_answer_id = $answer->id;
        $this->save();
    }

    /**
     * This function is invoked when user first time upvote or down vote for creating new record of user_id and question_id
     *
     * @param integer $vote
     * @return void
     */
    public function vote(int $vote)
    {
        //insert the record in the bridging table:
        $this->votes()->attach(auth()->id(), ['vote' => $vote]);

        //add or subtract the count into the votes_count of question table:
        if ($vote < 0) {
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
        }
    }


    /**
     * This function is used to update the user vote (for example: votes_count = 10 of questions table  and user already up-voted and now down voting so question is down voted by 2  and vice versa for already down-voted and now up-voting so question is upvoted by 2 (1 for user undo his action and 1 for he is upvoting ))
     *
     * @param integer $vote
     * @return void
     */
    public function updateVote(int $vote)
    {
        //update the record:
        $this->votes()->updateExistingPivot(auth()->id(), ['vote' => $vote]); //updateExistingPivot() is used when we want to update the column of bridging table except from user_id and vote_id.
        if ($vote < 0) {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }


    /********************************************************************END OF HELPER FUNCTIONS**************************************************************/
}
