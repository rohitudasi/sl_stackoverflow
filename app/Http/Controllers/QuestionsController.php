<?php

namespace App\Http\Controllers;

use App\Question;
use App\Http\Requests\questions\CreateQuestion;
use Illuminate\Support\Facades\Gate;


use Illuminate\Http\Request;

class QuestionsController extends Controller
{

    public function __Construct()
    {
        $this->middleware(['auth'])->only(['create', 'store']);
    }

    public function index()
    {

        $questions = Question::with('owner')
            ->latest()
            ->paginate(10);

        return view('questions.index', compact([
            'questions',
        ]));
    }

    public function create()
    {
        //
        app('debugbar')->disable();
        return view('questions.create');
    }

    public function store(Request $request)
    {
        //
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);
        session()->flash('success', "Question has been added successfully");
        return redirect(route('questions.index'));
    }

    public function show(Question $question)
    {
        //
        $question->increment('views_count');
        return view('questions.show', compact([
            'question'
        ]));
    }

    public function edit(Question $question)
    {

        // if (Gate::allows('update-question', $question)) {

        if ($this->authorize('update', $question)) {
            return view('questions.edit', compact([
                'question',
            ]));
        }

        // }


        abort(403);
    }


    public function update(Request $request, Question $question)
    {


        // if (Gate::allows('update-question', $question)) {

        if ($this->authorize('update', $question)) {
            $question->update([
                'title' => request('title'),
                'body' => $request->body
            ]);
            session()->flash('success', "Question has been Updated successfully");
            return redirect(route('questions.index'));
        }

        // }
        abort(403);
    }

    public function destroy(Question $question)
    {
        //
        // if (auth()->user()->can('delete-question', $question)) {
        // above if is used with gates now i am commenting it because i have used policies here:
        if ($this->authorize('delete', $question)) {
            $question->delete();
            session()->flash('success', "Question has been successfully deleted!");
            return redirect(route('questions.index'));
        }


        // }
        abort(403);
    }
}
