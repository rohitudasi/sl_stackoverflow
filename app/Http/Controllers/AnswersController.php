<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use App\Question;
use App\Http\Requests\answers\CreateAnswerRequest;
use App\Http\Requests\answers\UpdateAnswerRequest;
use App\Notifications\NewReplyAdded;

class AnswersController extends Controller
{

    public function store(CreateAnswerRequest $request, Question $question)
    {
        //
        $question->answers()->create([
            'body' => $request->body,
            'user_id' => auth()->id()
        ]);
        $question->owner->notify(new NewReplyAdded($question));
        session()->flash('success', 'Your Answer has ben submitted successfully!');
        return redirect($question->url);
    }

    public function edit(Question $question, Answer $answer)
    {
        //
        return view('answers.edit', compact([
            'answer',
            'question'
        ]));
    }

    public function update(UpdateAnswerRequest $request, Question $question, Answer $answer)
    {
        //
        $answer->update([
            'body' => $request->body,
        ]);

        session()->flash('success', 'Answer Updated Successfully');
        return redirect($question->url);
    }


    public function destroy(Question $question, Answer $answer)
    {
        //
        $this->authorize('delete', $answer);
        $answer->delete();
        session()->flash('success', 'answer has been deleted successfuly!');
        return redirect(route('questions.index'));
    }

    public function bestAnswer(Answer $answer)
    {
        $this->authorize('markAsBest', $answer);
        if ($answer->question->best_answer_id === $answer->id) {
            $answer->question->undoBestAnswer();
            session()->flash('success', 'Answer has been unmarked Best');
            return redirect($answer->question->url);
        }

        $answer->question->markBestAnswer($answer);
        session()->flash('success', 'Answer has been marked Best');
        return redirect($answer->question->url);
    }
}
