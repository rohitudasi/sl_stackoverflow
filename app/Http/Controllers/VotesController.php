<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;

class VotesController extends Controller
{
    //
    public function voteQuestion(Question $question, int $vote)
    {
        //Either user has already voted and again updating his vote:
        if (auth()->user()->hasVoteForQuestion($question)) {
            $question->updateVote($vote);
        } else {
            //user is voting for the first time:
            $question->Vote($vote);
        }
        return redirect()->back();
    }



    public function voteAnswer(Answer $answer, int $vote)
    {


        //Either user has already voted and again updating his vote:
        if (auth()->user()->hasVoteForAnswer($answer)) {
            $answer->updateVote($vote);
        } else {
            //user is voting for the first time:
            $answer->Vote($vote);
        }
        return redirect()->back();
    }
}
