@extends('layouts.app');

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    All Questions
                <a href="{{route('questions.create')}}" class="btn btn-sm btn-primary">Add Question</a>
                </div>
                @foreach($questions as $question)
                    <div class="card-body">
                        <div class="media">
                            <div class="d-flex flex-column statistics">

                                <div class="votes text-center mb-3">
                                <strong class="d-block">{{$question->votes_count}}</strong>
                                    Votes
                                </div>

                                <div class="answers text-center mb-3">
                                <strong class="d-block answers {{$question->answers_style}}">{{$question->answers_count}}</strong>
                                    Answers
                                </div>

                                <div class="views text-center">
                                <strong class="d-block">{{$question->views_count}}</strong>
                                Views
                                </div>

                            </div>

                            <div class="media-body">

                                <div class="d-flex justify-content-between">
                                    <h4>
                                        <a href="{{ $question->url }}">{{$question->title}}</a>
                                    </h4>

                                    <div class="d-flex justify-content-between">

                            <!---------------------------------------------------------To Apply Gates Below Code----------------------------------------------------->

                                                    <!-- If u want to use can() method using if of php so u have to call like below -->

                                                    {{-- @if(auth()>user() && auth()->user()->can('update-post'),$question) <!-- CODE GOES HERE --> @endif --}}

                                                     {{-- If u want to use @can method of blade u have to call like below  --}}

                                                    {{-- @can('update-question',$question)) <!--CODE GOES HERE --> @endcan--}}


                                                <!------------------------------------1-method---USING GATES CODE -------------using if---------------->

                                            @if(auth()->user() && auth()->user()->can('update-post',$question))
                                                <div class="mr-2">
                                                     <a href=" {{route('questions.edit',$question->id)}} " class="btn btn-sm btn-outline-primary">Edit</a>
                                                </div>
                                            @endif



                                                    <!------------------------------------2-method---USING GATES CODE -------------using can---------------->

                                            @can('update-question',$question)
                                                <div class="mr-2">
                                                <a href=" {{route('questions.edit',$question->id)}} " class="btn btn-sm btn-outline-primary">Edit</a>
                                            </div>
                                            @endcan

                            <!---------------------------------------------------------To Apply Gates Below Code----------------------------------------------------->

                                            <!-- for using policy we have to hit the command (php artisan make:policy policy_name --model="model_name") -->
                                            <!-- This will create the policy class in the App/policy/policy_name
                                            inside this call we have already craeted methods same as controller
                                            we have to just over-ride the methods and return true or false depending uson teh condition
                                            After writing condition we have to register it in App/provide/auth-provider -->


                                            @can('update',$question)
                                                <div class="mr-2">
                                                    <a href=" {{route('questions.edit',$question->id)}} " class="btn btn-sm btn-outline-primary">Edit</a>
                                                </div>

                                            @endcan



                                        @can('delete',$question)

                                            <form action="{{route('questions.destroy',$question->id)}}" method="POST">
                                                @csrf()
                                                @method('DELETE')
                                                <button type ="submit" onclick="return confirm('Are you sure you want to delete?');"class="btn btn-sm btn-outline-danger">Delete</button>
                                            </form>

                                        @endcan


                                    </div>

                                </div>
                                <p>
                                    Asked By: <a href="">{{$question->owner->name}}</a>
                                    <span class="text-muted">Asked: {{$question->created_date}}</span>
                                </p>
                                <p>
                                    {!! Str::limit($question->title,255) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                <div class="card-footer">
                    {{$questions->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

