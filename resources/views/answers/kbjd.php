<div class="d-flex">
    <div class="d-flex flex-column">

        <div>
            <a href="" title="Up Vote" class="vote-up d-block text-center text-dark">
                <i class="fa fa-caret-up fa-3x" aria-hidden="true"></i>
            </a>
            <h4 class="votes-count text-muted text-center m-0">45</h4>
            <a href="" title="Down Vote" class="vote-down d-block text-center text-black-50">
                <i class="fa fa-caret-down fa-3x" aria-hidden="true"></i>
            </a>
        </div>

        <div class="mt-2">
            <a href="" title="Mark as favorite" class="favorite d-block text-center mb-2"><i class="fa fa-star fa-2x text-dark" aria-hidden="true"></i></a>
            <h4 class="votes-count m-0 text-center">123</h4>
        </div>
    </div>
</div>
