<!--This view has all answers,  and this view is included in the show view of questions --->

<div class="row-mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-0">
                    {{ Str::plural('Answer', $question->answers_count) }}
                </h3>
            </div>

            <div class="card-body">
                @forelse($question->answers as $answer)
                <div class="answer-container d-flex">
                    <!--Answer Stats -->
                    <div class="answer-status d-flex flex-column">
                        @can('vote',$answer)

                                        <form action="{{route('answers.vote',[$answer->id, 1])}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn {{auth()->user()->hasanswerUpVote($answer) ? 'text-dark' : 'text-black-50'}}">
                                                <i class="fa fa-caret-up fa-3x" aria-hidden="true"></i>
                                            </button>
                                        </form>

                                    @else
                                    <a href="{{route('login')}}" title="Up Vote" class="vote-up d-block text=black-50">

                                    </a>
                                    @endcan
                                     <h4 class="votes-count text-muted text-center m-0">{{$answer->votes_count}}</h4>

                                     @can('vote',$answer)

                                        <form action="{{route('answers.vote',[$answer->id, -1])}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn {{auth()->user()->hasAnswerDownVote($answer) ? 'text-dark' : 'text-black-50'}}">
                                                <i class="fa fa-caret-down fa-3x" aria-hidden="true"></i>
                                            </button>
                                        </form>

                                    @else
                                    <a href="{{route('login')}}" title="down Vote" class="vote-down d-block text=black-50">

                                    </a>
                                    @endcan

                        <div class="mt-2">
                        @can('markAsBest',$answer)
                            <form action="{{route('answers.bestAnswer',$answer->id)}}" method="POST">
                            @csrf
                            @method('PUT')

                                <button type="submit" class="btn {{ $answer->best_answer_status }}"><i class="fa fa-check fa-2x"></i></button>
                            </form>
                            @else
                                @if($answer->isBest)
                                    <i class="fa fa-check fa-2x d-block text-success mb-2"></i>
                                @endif
                        @endcan

                    </div>
                    </div>
                    <!-- End Answer Stats -->

                    <!-- Answer Content -->
                    <div class="answer-content w-100">
                            {!! $answer->body !!}

                    <div class="d-flex justify-content-between">
                        <div>
                            <div class="d-flex">
                                @can('update',$answer)
                                <a href="{{ route('questions.answers.edit',[$question->id, $answer->id]) }}" class="btn btn-sm btn-primary">Edit</a>
                                @endcan


                                @can('delete',$answer)
                                    <form action="{{route('questions.answers.destroy',[$question->id,$answer->id])}}" method="POST">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" onclick="return confirm('Are you sure u want to delete the post');" class="btn ml-3 btn-danger">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                Answered {{ $answer->created_date }}
                            </div>
                            <div class="d-flex mb-2">
                                <div>
                                    <img src="{{$answer->author->avatar}}" alt="">
                                </div>
                                <div class="mt-2 ml-2">
                                    {{$answer->author->name}}
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>


                <hr>
                @empty
                    <h1>No Answers</h1>
                @endforelse
            </div>
        </div>
    </div>
</div>
